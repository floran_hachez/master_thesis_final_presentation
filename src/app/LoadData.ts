/// <reference path='../../typings/d3/d3.d.ts'/>;
/// <reference path='../../typings/underscore/underscore.d.ts'/>;
module charting {

    interface CDRRow {
        source: string;
        target: string;
        startTime: string;
        duration: string;
    }

    export class IntMap<V>{
        [index:number]:V;
    }

    export function loadCdrs( file: string ): d3.DsvXhr<EpidemicLink<EpidemicNode>>{
        var nodes = new IntMap<EpidemicNode>();
        var count =0;
        console.log("---------Map-----------");
        return d3.csv(file)
        .row(function(d: {[key: string]: string}) { 
            // Nodes
            let s = parseInt(d["source"]);
            let t = parseInt(d["target"]);

            // Create the new node if need be
            if(!nodes[s]){
                nodes[s] = new EpidemicNode(s);
            }
            if(!nodes[t]){
                nodes[t] = new EpidemicNode(t);
            }
            
            // Interval
            let infection = false;
            if(d["infection"] && d["infection"]=="True"){
                infection = true;
            }
            let interval = new charting.EpidemicActiveInterval ( parseInt( d["startTime"] ), parseInt( d["duration"] ), infection);

            return new EpidemicLink ( nodes[s], nodes[t], [interval] );
        });
    }

    export function loadInfectionLogs( file: string ): d3.DsvXhr<InfectionLog>{
        return d3.csv(file)
        .row(function(d: {[key: string]: string}) { 

            return new  InfectionLog( 
                                       parseInt( d["node"] ),
                                       parseInt( d["infection_time"] ),
                                       parseInt( d["recovery_time"] )
                                    );
        });
    }

    export function fillSpreadingPoints( points: SpreadingPoint[] ){
        let filledPoints: SpreadingPoint[] = [];

        let i=0;
        for(; i< points.length-1; i++){
            let current = points[i] 
            let next = points[i+1] 
            filledPoints.push(current);
            let t=current.timestamp;
            for(;t< next.timestamp; t++){
                filledPoints.push( new SpreadingPoint( t, current.infected, current.susceptible, current.recovered));
            }
        }

        // Push the last point
        filledPoints.push( points[points.length-1] );

        return filledPoints;
    }

    export function loadSpreadingPoints( file: string ): d3.DsvXhr<SpreadingPoint>{
        return d3.csv(file)
        .row(function(d: {[key: string]: string}) { 
            return new  SpreadingPoint( 
                                       parseInt( d["timestamp"] ),
                                       parseFloat( d["infected"] ),
                                       parseFloat( d["susceptible"] ),
                                       parseFloat( d["recovered"] ));
        });
    }
}
