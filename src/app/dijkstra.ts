/// <reference path='../../typings/d3/d3.d.ts'/>;
/// <reference path='../../typings/underscore/underscore.d.ts'/>;

module charting {
    interface NodeDijkstra extends Node {
        distance:number;
        links:Link<NodeDijkstra>[];
        visited: boolean;
    }

    export class Dijkstra<L extends Link<N>, N extends NodeDijkstra>  {
        nodes:N[];
        edges:L[];
        source:N;

        constructor( nodes: N[], edges: L[]){
            this.nodes = nodes;
            this.edges = edges;
        }

        run( src: N ){
            this.source = src;
            var unvisited = [];

            this.nodes.forEach(function (d) {
                if (d != src) {
                    d.distance = Infinity;
                    unvisited.push(d);
                    d.visited = false;
                }
            });

            var current = src;
            current.distance = 0;

            while(!(unvisited.length == 0 || current.distance == Infinity)){
                current.visited = true;
                current.links.forEach(function(link) {
                    var tar = link.target;
                    if (!tar.visited) {
                        var dist = current.distance + 1;
                        tar.distance = Math.min(dist, tar.distance);
                    }
                    var tar = link.source;
                    if (!tar.visited) {
                        var dist = current.distance + 1;
                        tar.distance = Math.min(dist, tar.distance);
                    }
                });
                unvisited.sort(function(a, b) {
                    return b.distance - a.distance 
                });
                if(unvisited.length > 0){
                    current = unvisited.pop()
                }
            }
        }
    }
}
