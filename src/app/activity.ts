/// <reference path='../../typings/d3/d3.d.ts'/>;
/// <reference path='../../typings/underscore/underscore.d.ts'/>;
module charting {
    export function riemann( a: number, epsilon: number= Math.pow( 10, -5 ) ): number {
        console.log(epsilon);
        let stop = Math.round(Math.ceil( Math.pow( epsilon, -1.0/a )));
        console.log("Stop : " + stop);
        let r = 0;
        for( var k of _.range(1, stop )){
            r+= Math.pow( k, -a );
        }
        return r;
    }

    export function discretePowerLawDistribution( x: number, a: number, coef: number): number {
        return Math.pow( x, -a )/coef;
    }

    export function geometricDistribution( nTrials: number, p: number ): number {
        return p* Math.pow( 1-p, nTrials -1 );
    }

}
