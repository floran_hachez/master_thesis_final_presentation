/// <reference path='../../typings/d3/d3.d.ts'/>;
/// <reference path='../../typings/underscore/underscore.d.ts'/>;
module charting{

    export class EpidemicNode extends DegreeNode {
        infectedIntervals: ActiveInterval[] = [];
        outgoingInteractions: ActiveInterval[] = [];

        constructor(index: number){
            super(index);
        }

        isActive( time: number ): boolean {
            return _.some( this.infectedIntervals, (i) => i.contains( time ));
        }
    }

    export class SpreadingNetwork extends DynamicNetwork<EpidemicLink<EpidemicNode>, EpidemicNode> {
        constructor( nodes: EpidemicNode[], links: EpidemicLink<EpidemicNode>[], infectionLogs: InfectionLog[]) {
            super(nodes, links);
            this.decorateNodesWithInfectionLogs( infectionLogs );
            this.decorateNodesWithActivityTime();
        }

        private decorateNodesWithActivityTime(){
            for(let l of this.links){
                let n = l.source;
                n.outgoingInteractions = n.outgoingInteractions.concat(l.activeIntervals);
            }
        }


        private decorateNodesWithInfectionLogs( infectionLogs: InfectionLog[] ){

            for(let log of infectionLogs){
                let n = _.find( this.nodes, (n) => n.index == log.nodeId );
                n.infectedIntervals.push( log.infectionInterval );
            }
            console.log(this.nodes);
        }

    }
            

    export class SpreadingNetworkChart extends TimeNetworkChart<EpidemicLink<EpidemicNode>, EpidemicNode> {
        linkClass = (l, t) => {
            var stateClass = 'active-link';
            if( !l.isActive(t) ) {
                stateClass = 'not-active-link';
            }else{
                stateClass += (l.isInfected(t) ? ' infected-link': ' not-infected-link');
            }
            return stateClass;
        };

        nodeClass = (n, t) => {
            var stateClass = 'infected_node';
            if( !n.isActive(t) ) {
                stateClass = 'susceptible_node';
            }
            return stateClass + ' ' + n.index;
        };

        constructor( config: ChartConfig, container: any, network: DynamicNetwork<EpidemicLink<EpidemicNode>, EpidemicNode>) {
            super(config, container, network);

        }

    }

    export enum EpidemicModel {
        SI,
        SIS,
        SIR
    }

    export class EpidemicActiveInterval extends ActiveInterval {
        infection: boolean;

        constructor( startTime: number, duration: number, infection: boolean ) {
            super( startTime, duration );
            this.infection = infection;
        }

    }

    export class EpidemicLink<N extends DegreeNode> extends LinkWithTime<N> {
        activeIntervals: EpidemicActiveInterval[];
        constructor( source: N, target: N, activeIntervals: EpidemicActiveInterval[] ){
            super(source, target, activeIntervals);
        }

        isInfected( time: number ): boolean {
            return _.some( this.activeIntervals, (i) => i.contains( time ) && i.infection );
        }
        
    }

    export class InfectionLog {
        nodeId: number;
        infectionTimestamp: number;
        recoveryTimestamp: number;

        infectionInterval: ActiveInterval;
        
        constructor( nodeId: number, infectionTimestamp: number, recoveryTimestamp: number){
            this.nodeId = nodeId;
            this.infectionTimestamp = infectionTimestamp;
            this.recoveryTimestamp = recoveryTimestamp;
            let duration: number = recoveryTimestamp - infectionTimestamp;
            this.infectionInterval = new ActiveInterval( infectionTimestamp, duration );
        }
    }

    export class SpreadingPoint extends Point {
        timestamp: number;
        infected: number;
        recovered: number;
        susceptible: number;
        model: EpidemicModel;

        constructor(timestamp: number, infected: number, recovered: number, susceptible: number){
            super( timestamp, infected );
            this.timestamp = timestamp;
            this.infected = infected;
            this.susceptible = 1 - infected;
            this.model = EpidemicModel.SI;
            if( susceptible ){ // SIS case
                this.susceptible =  susceptible;
                this.model = EpidemicModel.SIS;
            }
            this.recovered = 0;
            if( recovered ){ // SIR case
                this.recovered = recovered;
                this.susceptible = 1 - this.recovered - this.infected;
                this.model = EpidemicModel.SIR;
            }
        }
    }

}
