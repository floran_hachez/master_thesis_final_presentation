/// <reference path='../../typings/jquery/jquery.d.ts'/>;

module charting {
    export class ChartConfig {
        width: number;
        height: number;

        constructor( width: number, height: number ) {
            this.width = width;
            this.height = height;
        }
    }

    export class Dim {
        top; right; bottom; left;
        constructor( top= 20, right= 20, bottom=20, left= 20){
            this.top = top;
            this.right = right;
            this.bottom = bottom;
            this.left = left;
        }
    }
    export var marginDim = new Dim( 20, 20, 20, 20);
    export var paddingDim = new Dim( 60, 60, 60, 60);

    export class ChartConfigWithMargin extends ChartConfig {
        margin: Dim;
        padding: Dim;
        outerWidth: number;
        outerHeight: number;
        innerWidth: number;
        innerHeight: number;

        constructor( outerWidth: number, outerHeight: number, margin: Dim, padding: Dim ){
            super( outerWidth - margin.left - margin.right - padding.left - padding.right,
                  outerHeight - margin.top - margin.bottom - padding.top - padding.bottom);
            this.padding = padding;
            this.margin = margin;
            this.outerWidth = outerWidth;
            this.outerHeight = outerHeight;
            this.innerWidth = outerWidth - margin.left - margin.right;
            this.innerHeight = outerHeight - margin.top - margin.bottom;
            this.width = this.innerWidth - padding.left - padding.right;
            this.height = this.innerHeight - padding.top - padding.bottom;
        }
    }

    export function chartConfigFromContainer( id: string ): ChartConfig {
        let node = $(id);
        return new ChartConfig( node.width(), node.height());
    }
}
