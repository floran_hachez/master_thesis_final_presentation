module charting {

    import ChartConfig = charting.ChartConfig;
    import Scale = charting.Scale;
    import ChartConfigWithMargin = charting.ChartConfigWithMargin;

    export class ChartAxis {
        group: d3.Selection<any>;
        config: ChartConfig;
        scale: Scale<number, number>;
        axis: d3.svg.Axis;
        max: number;
        min: number;

        constructor( config: ChartConfigWithMargin, container: d3.Selection<any>, min: number, max: number, isLog: boolean){
            if( isLog ){
                this.scale = d3.scale.log<number, number>()
                .base(10) ;
            }else{
                this.scale = d3.scale.linear<number, number>();
            }
            this.scale.domain([min, max]);
            this.max = max;
            this.min = min;
            this.group = container.append("g")
            this.axis = d3.svg.axis()
                .ticks(5)
                .scale(this.scale);
        }

        update( min: number, max: number ){
            this.scale.domain( [min, max] );
            this.group.call( this.axis );
        }

        clipToDomain( x: number ){
            let [min, max] = this.scale.domain();
            if( x<min ){
                return min;
            }
            if( x>max ){
                return max;
            }
            return x;
        }

        draw() {
            this.group.call( this.axis );
        }

    }

    export class XAxis extends ChartAxis {
        constructor( config: ChartConfigWithMargin, container: d3.Selection<any>, min: number, max: number, label: string, isLog: boolean){
            super( config, container, min, max, isLog)
            this.scale.range([0, config.width]);
            this.axis.orient('bottom');
            this.group.attr("transform", "translate(0," + config.height + ")") // Put at the bottom
            // now add titles to the axes
            this.group.append("text")
            .attr("class", "axis-label")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate("+ (config.width/2) +","+(config.padding.bottom/1.5)+")")  // centre below axis
            .text(label);

        }
    }

    export class YAxis extends ChartAxis {
        constructor( config: ChartConfigWithMargin, container: d3.Selection<any>, min: number, max: number, label: string, isLog: boolean ){
            super( config, container, min, max, isLog)
            console.log(config);
            this.scale.range([config.height, 0]);
            this.axis.orient('left');
            this.group.append("text")
            .attr("class", "axis-label")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate("+ -(config.padding.left/1.5) +","+(config.height/2)+")rotate(-90)")  // text is drawn off the screen top left, move down and out and rotate
            .text(label);
        }
    }

}
