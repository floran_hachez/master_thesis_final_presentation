
module charting {
    
    export class SISChart extends Chart implements EventListener {
        
        private data: SpreadingPoint[];
        private R0_input: JQuery;
        private i0: number;
        private maxTime: number;
        private playTimeout: any;
        private playPauseButton: PlayPauseButton;
        private R0AnimationValues: number[];
        private animationIndex: number;
        private static beta = 0.01;
        private static ck = 10;

        constructor(config: ChartConfigWithMargin, container: string, maxTime: number, i0: number, R0_input: string, playPauseButton: PlayPauseButton, R0AnimationValues: number[]) {
            super(config, container, "Time", 0, maxTime, false, "Fraction of Infected", 0, 1, false);
            this.R0_input = $(R0_input);
            this.i0 = i0;
            this.maxTime = maxTime;
            this.animationIndex = 0;
            this.playPauseButton = playPauseButton;
            this.R0AnimationValues = R0AnimationValues;

            // Register listener on R0
            this.R0_input.on("change", (function() {
                this.playPauseButton.stopIfPlaying();
                this.refresh(this.R0_input.val());
            }).bind(this));
            this.playPauseButton.addListener(this);
        }
        
        private computeSIS(R0) {
            console.log("R0 is " + R0);
            let mu = SISChart.beta * SISChart.ck / R0;
            let sis = new ClassicalSIS(this.i0, SISChart.beta, mu, SISChart.ck);
            this.data = charting.fillSpreadingPoints(sis.evaluate_until(this.maxTime));
        }

        private refresh(R0) {
            this.computeSIS(R0);
            this.clearLines();
            this.R0_input.val(R0);
            this.drawAxis();
            this.addLine(new Line(this.data, "dummyLineName"))
        }

        public start() {
            this.refresh(this.R0_input.val());
        }


        public event(e: ControlEvent, argument?: any) {
            switch(e) {
                case ControlEvent.PLAY:
                    this.handlePlay();
                    break;
                case ControlEvent.PAUSE:
                    this.handlePause();
                    break;
                default:
                    throw new Error("Unhandled event: " + e);
            }
        }

        private handlePlay() {
            console.log("Play triggered");
            if( !this.playTimeout ) {
                let callback = () => {
                    this.animationIndex++;
                    if(this.animationIndex >= this.R0AnimationValues.length) {
                        this.animationIndex = 0;
                    }

                    let R0 = this.R0AnimationValues[this.animationIndex];
                    this.refresh(R0);
                };
                this.refresh(this.R0AnimationValues[0]);
                this.playTimeout = setInterval(callback.bind(this), 1500);
            }
        }

        private handlePause() {
            console.log("Pause triggered");
            if (this.playTimeout) {
                this.animationIndex = 0;
                clearTimeout(this.playTimeout);
                this.playTimeout = null;
                this.playPauseButton.stopIfPlaying();
            }
        }

    }

    export class ClassicalSIS {

        i0: number;
        beta: number;
        mu: number;
        ck: number;
        R0: number;

        public constructor(i0: number, beta: number, mu: number, ck: number) {
            this.i0 = i0;
            this.beta = beta;
            this.mu = mu;
            this.ck = ck;
            this.R0 = beta * ck / mu;
        }

        private isInteger = function(value) {
              return typeof value === "number" && 
                  isFinite(value) && 
                  Math.floor(value) === value;
        };

        public evaluate_until(tf: number, dt = 1): SpreadingPoint[] {
            var results = [];
            var t = 0;
            var i = this.i0;
            results.push(new SpreadingPoint(t, i, 0, 1-i));
            while(t <= tf) {
                if(i > 1.0)
                    throw new Error("Infection reached i = " + i);
                t += dt;
                i += dt * (this.beta * this.ck * i * (1-i) - i * this.mu);
                if(this.isInteger(t)) {
                    results.push(new SpreadingPoint(t, i, 0, 1-i));
                }
            }
            return results;
        }
    }
}

