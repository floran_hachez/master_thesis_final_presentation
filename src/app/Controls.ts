/// <reference path='../../typings/d3/d3.d.ts'/>;

module charting {

    export const enum ControlEvent {
        PLAY, PAUSE, NEW_TIME, RESET
    }

    export interface EventListener {
        event(e: ControlEvent, argument?: any): void
    }

    /* Mixin class used by all listenable classes */
    export class Listenable {
        listeners: EventListener[];

        public addListener(listener: EventListener) {
            this.listeners.push(listener);
        }

        public addListeners(listeners: EventListener[]) {
            this.listeners = this.listeners.concat(listeners);
        }

        public clearListeners() {
            this.listeners = [];
        }

        notify(event: ControlEvent, datum?: any) {
            this.listeners.forEach(l => l.event(event, datum));
        }
    }

    abstract class Button implements Listenable {
        protected ref: d3.Selection<any>;

        constructor(id: string) {
            this.ref = d3.select("#" + id);
            this.ref.on("click", this.onClick.bind(this));
        }

        // Listenable "implementation/declaration" (actual implementation comes as a Mixin)
        listeners: EventListener[] = [];
        addListener: (listener: EventListener) => void;
        addListeners: (listeners: EventListener[]) => void;
        clearListeners: () => void;
        notify: (event: ControlEvent, datum?: any) => void;

        public click() {
            this.onClick();
        }

        protected abstract onClick(): void;
    }
    applyMixins(Button, [Listenable]);


    export class ResetButton extends Button {

        protected onClick() {
            this.notify(ControlEvent.RESET)
        }
    }

    export class PlayPauseButton extends Button {
        
        private playing = false;

        protected onClick() {
            this.playing = !this.playing;
            if (this.playing) {
                this.ref.classed("yellow", true).classed("red", false);
                this.ref.select("i").html("pause");
            } else {
                this.ref.classed("red", true).classed("yellow", false);
                this.ref.select("i").html("play_arrow");
            }
            let ev = this.playing ? ControlEvent.PLAY : ControlEvent.PAUSE;
            this.notify(ev);
        }

        public stopIfPlaying() {
            if(this.playing) {
                this.ref.classed("red", true).classed("yellow", false);
                this.ref.select("i").html("play_arrow");
                this.notify(ControlEvent.PAUSE);
                this.playing = false;
            }
        }
    }


}



