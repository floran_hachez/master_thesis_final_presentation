/// <reference path='../../typings/d3/d3.d.ts'/>;
/// <reference path='../../typings/underscore/underscore.d.ts'/>;

module charting {
    
    export type Node = d3.layout.force.Node;
    export type Link<N> = d3.layout.force.Link<N>;
    type ToNumber<T> = (e: T) => number;
    type ToText<T> = (e: T) => string;

    export interface LinkWithWeight<N extends DegreeNode> extends Link<N> {
        weight: number;
    }

    export class ActiveInterval {
        startTime: number;
        duration: number;
        endTime: number;

        constructor( startTime: number, duration: number ) {
            this.startTime = startTime;
            this.duration = duration;
            this.endTime = startTime + duration;
        }

        contains( x: number ) {
            return x >= this.startTime && x < this.endTime;
        }
    }
    
    export class LinkWithTime<N extends DegreeNode> implements Link<N> {
        source: N;
        target: N;
        activeIntervals: ActiveInterval[];

        constructor( source: N, target: N, activeIntervals: ActiveInterval[] ){
            this.source = source;
            this.target = target;
            this.activeIntervals = activeIntervals;
        }

        isActive( time: number ): boolean {
            return _.some( this.activeIntervals, (i) => i.contains( time ));
        }
    }

   function nodeIndex(n: any) {
       if( !n.index ) {
           return parseInt( n );
       } else {
           return n.index;
       }
   }
    
    export class Network<L extends Link<N>, N extends Node> {
        nodes: N[];
        links: L[];
        uniqueLinks: L[];

        constructor( nodes: N[], links: L[]) {
            this.nodes = nodes;
            this.links = links;
            this.uniqueLinks = this.computeUniqueLinks();
        }

        protected linkId(l) {
                let maxN = 10000000000;
                let id: number;
                let a = l.source.index, b = l.target.index;
                if (a < b) {
                    id = a * maxN + b;
                }else {
                    id = b * maxN  + a;
                }
                return id;
        };

        centerNodes( x, y ) {
            console.log("center node in ("+x+", "+y+ ")");
            for( let n of this.nodes ){
                n.x = x;
                n.y = y;
            }
        }


        private computeUniqueLinks(): L[] {
            let linksData = this.links;
            let uniqLinksData = _.uniq( linksData, false, this.linkId);
            return uniqLinksData;
        }

    }

    export class DegreeNode implements Node {
        index: number;
        degree: number = 0;
        x:number = 0;
        y:number = 0;

        constructor( index: number ){
            this.index = index;
            this.degree = 0;
            this.x=0;
            this.y=0;
        }
    }

    export class DynamicNetwork<L extends LinkWithTime<N>, N extends DegreeNode> extends Network<L, N> {
        aggregatedLinks: L[] = [];

        constructor( nodes: N[], links: L[]) {
            super(nodes, links);
            this.aggregateLinks();
            this.computeDegree();
        }

        private aggregateLinks() {
            var linkMap = new IntMap<L>();

            for(var l of this.links) {
                let id = this.linkId( l );
                if( linkMap[id] ) {
                    // Already passed on this edge, let's aggregate
                    var fullLink = linkMap[id];
                    fullLink.activeIntervals = fullLink.activeIntervals.concat(l.activeIntervals);
                }else {
                    // First pass on the edge
                    linkMap[id] = l;
                    this.aggregatedLinks.push(l);
                }
            }
        }

        private computeDegree(){
            for( let l of this.aggregatedLinks ){
                l.source.degree ++;
                l.target.degree ++;
            }
        }
    }

    export function networkFromEdgeList<N extends DegreeNode, L extends LinkWithTime<N>>( edges: L[] ): DynamicNetwork<L, N> {
        let nodeMap = new IntMap<boolean>();
        let nodes: N[] = [];

        function insertNode( n: N ) {
            let id = n.index;
            if (!nodeMap[id] ) {
                nodes.push(n);
                nodeMap[id] = true;
            }
        } 

        for( var e of edges ) {
            insertNode( e.source );
            insertNode( e.target );
        }

        return new DynamicNetwork( nodes, edges );
    }

    export function completeNetwork( n: number ): Network<Link<Node>, Node> {
        let indexes: number[] = _.range(n);
        let nodes: Node[] = _.map( indexes, (i) => { return <Node>{ index: i, group:i, name: 'node' + i }; });
        let links: Link<Node>[] = [];
        for ( let s of nodes ) {
            for( let t of nodes ) {
                if ( s.index > t.index ) {
                    links.push( <Link<Node>> { source: s, target: t, weight:5 });
                }
            }
        }
        return new Network( nodes, links );
    }

    export abstract class NetworkChart<L extends Link<N>, N extends Node> {
        protected _svg: d3.Selection<any>;
        protected _group: d3.Selection<any>;
        config: ChartConfig;

        // Layout
        force: d3.layout.Force<L, N> ;

        zoom = d3.behavior.zoom().scaleExtent([0.1, 10]);

        zoomed() {
            // Capture the group to move !
            let group: d3.Selection<any> = this._group;
            return () => {
                console.log("Let's zoom");
                let event = <d3.ZoomEvent>d3.event;
                group.attr("transform", "translate(" + event.translate + ")scale(" + event.scale + ")") ;
            };
        };

        // Mapping
        linkToStrokeWidth: ToNumber<L>;
        nodeToName: ToText<N>;
        nodeToSize: ToNumber<N> = (n) => 7;

        constructor( config: ChartConfig, container: any) {
            this.config = config;
            this.init( container );
            this.force = d3.layout.force<L, N>()
                    .gravity(.05)
                    .linkDistance(60)
                    .charge(-50)
                    .size([this.config.width, this.config.height]);
        }

        private init( container: any ) {
            this._svg = d3.select( container )
            this._group = this._svg.append('g');
            this.zoom.on("zoom", this.zoomed());
            this._svg.call(this.zoom);
            this._group.on("zoom",null);
        }

        scale( scalingFactor: number, translationFactor: [number,number]){
            this.zoom.scale(scalingFactor);
            this.zoom.translate(translationFactor);
            this.zoom.event(this._svg.transition().duration(500));
        }
    }

    export class StaticNetworkChart<L extends LinkWithWeight<N>, N extends DegreeNode> extends NetworkChart<L, N> {
        linkToStrokeWidth: ToNumber<L> = (d) => {return 1;};
        nodeToName: ToText<N> =  (d) => {return 'standard-size'; };
        nodeToColor: ToText<N> =  (d) => {return ''; };
        nodeToClass: ToText<N> =  (n) => { return 'node basic-node'};

        constructor( config: ChartConfig, container: any) {
            super(config, container);
        }

        draw ( nodesData: N[], linksData: L[]) {
            this.force.nodes(nodesData)
                .links(linksData);

            let link = this._group.selectAll('.link')
                .data(linksData)
                .enter().append('line')
                .attr('class', 'link')
                .style('stroke-width', this.linkToStrokeWidth);

            let node = this._group.selectAll('.node')
                .data(nodesData)
                .enter().append('g')
                .attr('class', this.nodeToClass);
                //.call(this.force.drag);

            node.append('circle')
                .attr('r', this.nodeToSize)
                .attr('fill', this.nodeToColor)
                .attr('class', 'node-circle');
            /*
            node.append('text')
                .attr('class', 'node-name')
                .attr('dx', "-.30em")
                .attr('dy', ".35em")
                .text( this.nodeToName );
            */
            this.force.on('tick', function() {
                link.attr('x1', function(d) { return d.source.x; })
                .attr('y1', function(d) { return d.source.y; })
                .attr('x2', function(d) { return d.target.x; })
                .attr('y2', function(d) { return d.target.y; });
                node.attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; });
            });

            this.force.start();
        }
    }

    export class TimeNetworkChart<L extends LinkWithTime<N>, N extends DegreeNode> extends NetworkChart<L, N> {
        network: DynamicNetwork<L, N>;

        linkClass: (L, number) => string = (l, t) => {
            var stateClass = 'active-link';
            if( !l.isActive(t) ) {
                stateClass = 'not-active-link';
            }
            return stateClass;
        };

        nodeClass: (N, number) => string = (n, t) => {
            return 'basic-node';
        };

        linkStroke: (L, number) => string = (l, t) => {
            var stroke = '';
            if( !l.isActive(t) ) {
                stroke = '5,5';
            }
            return stroke;
        };

        constructor( config: ChartConfig, container: any, network: DynamicNetwork<L, N>) {
            super(config, container);
            this.network = network;
            this.initForce();
            this.initLinks();
            // The nodes should be draw after the links
            this.initNodes();
            this.centerNode();
        }

        private centerNode() {
            for( let n of this.network.nodes ){
                n.x = this.config.width /2.0;
                n.y = this.config.height /2.0;
            }
        }

        /**
         * Initialise the force layout with all the edges and all the node
         */
        private initForce() {
            this.force.nodes(this.network.nodes)
                .links(this.network.links);
        }

        /**
         * Create all the links, there shoudn't be new nodes or removed one. 
         * Only the state and class should change
         */
        private initLinks() {
            let link = this._group.selectAll('.link')
                .data(this.network.aggregatedLinks)
                .enter()
                .append('line')
                .attr('class', 'link');
        }

        /**
         * Create all the nodes, there shoudn't be new nodes or removed one. 
         * Only the state should change
         */
        private initNodes() {
            let nodesData = this.network.nodes;

            let node = this._group.selectAll('.node')
                .data(nodesData)
                .enter().append('g')
                .attr('class', (n) => 'node')
                //.call(this.force.drag);

            node.append('circle')
                .attr('r', this.nodeToSize)
                .attr('class', 'node-circle');
            node.append('text')
                .attr('class', 'node-name')
                .text( this.nodeToName );
            node.on('click', (e: N, i: number) => { 
                console.log(e);
            });
        }

        public start() {
            this.force.start();
            this.draw(0);
        }

        public draw ( time: number ) {
            //console.log("Draw for time " + time);

            // Update links
            let link = this._group.selectAll('.link')
                .data(this.network.aggregatedLinks)
                .attr('class', (l) => 'link ' + this.linkClass(l, time))
                .attr('stroke-dasharray',  (l) => this.linkStroke(l, time))
                .style('stroke-width', this.linkToStrokeWidth);

            let node = this._group.selectAll('.node').data(this.network.nodes)
                .attr('class', (n) => 'node '+this.nodeClass(n, time));

            node.selectAll('circle')
                .attr('r', this.nodeToSize);

            this.force.on('tick', function() {
                // All the active link should move with the layout
                link
                .attr('x1', function(d) { return +d.source.x; })
                .attr('y1', function(d) { return d.source.y; })
                .attr('x2', function(d) { return d.target.x; })
                .attr('y2', function(d) { return d.target.y; });
                node.attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; });
            });
        }

    }

}
