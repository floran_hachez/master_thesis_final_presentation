/// <reference path='../../typings/d3/d3.d.ts'/>;
/// <reference path='../../typings/underscore/underscore.d.ts'/>;

module charting {

    export class ControllableDynamicNetworkChart<L extends LinkWithTime<N>, N extends DegreeNode> implements EventListener {
        
        private networkChart: TimeNetworkChart<L,N>[];
        private timeChart: TimeChart;
        private playPause: PlayPauseButton;
        private reset: ResetButton;
        private playTimeout: number;
        private time: number;
        private timeTextRef: d3.Selection<any>;

        constructor(networkChart: TimeNetworkChart<L,N>[], timeChart: TimeChart, playPause: PlayPauseButton, reset: ResetButton, timeTextId: string) {
            this.networkChart = networkChart;
            this.timeChart = timeChart;
            this.playPause = playPause;
            this.reset = reset;
            this.time = 0;
            this.timeTextRef = d3.select("#" + timeTextId);

            // Register listeners
            this.timeChart.timeBar.addListener(this);
            this.playPause.addListener(this);
            this.reset.addListener(this);
        }

        public start() {
            _.forEach( this.networkChart, (chart) => chart.start());
            this.timeChart.start();
        }

        event(e: ControlEvent, argument?: any) {
            switch(e) {
                case ControlEvent.PLAY:
                    this.handlePlay();
                    break;
                case ControlEvent.PAUSE:
                    this.handlePause();
                    break;
                case ControlEvent.RESET:
                    this.handleReset();
                    break;
                case ControlEvent.NEW_TIME:
                    this.handleNewTime(argument);
                    break;
                default:
                    throw new Error("Unhandled event: " + e);
            }
        }

        private handlePlay() {
            console.log("Play triggered");
            if( !this.playTimeout ) {
                let callback = () => {
                    if(this.time > this.timeChart.xAxis.max) {
                        clearTimeout(this.playTimeout);
                    }
                    this.time++;
                    this.redraw();
                };
                this.playTimeout = setInterval(callback.bind(this), 1000);
            }
        }

        private handlePause() {
            if (this.playTimeout) {
                console.log("Pause triggered");
                clearTimeout(this.playTimeout);
                this.playTimeout = null;
            }
        }

        private handleReset() {
            console.log("Reset triggered");
            if(this.playTimeout) { // If playing, force a pause
                this.playPause.click();
            }
            this.time = 0;
            this.redraw();
        }

        handleNewTime(time: number) {
            console.log("New time Triggered: " + time);
            this.time = time;
            this.redraw();
        }

        private redraw() {
            let time = this.time;
            _.forEach( this.networkChart, (chart) => chart.draw(time));
            this.timeTextRef.text(this.time);
            this.timeChart.draw(this.time);
        }

    }

}
