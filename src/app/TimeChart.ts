/// <reference path='../../typings/d3/d3.d.ts'/>;
/// <reference path='../../typings/underscore/underscore.d.ts'/>;
/// <reference path='../../typings/reveal/reveal.d.ts'/>;

module charting {

    export type Scale<Range, Output> = d3.scale.Log<Range, Output> | d3.scale.Linear<Range, Output>;

    export class Point {
        x:number;
        y:number;
        constructor( x: number, y: number ){
            this.x = x;
            this.y = y;
        }
    }
    
    export class TimeBar implements Listenable {
        private _group: d3.Selection<any>;
        chart: Chart;
        timeBar = new Point(0, 1);

        constructor( chart: Chart, group: d3.Selection<any> ){
            this.chart = chart;
            this._group = group;
        }

        draw( t: number ){
            this.timeBar.x = t;

            var rect = this._group.selectAll('rect')
            .data([this.timeBar]);

            rect.enter()
            .append('rect');


            let chart = this.chart; // Allow the reference to this from functions
            rect.attr('class', 'time-bar')
            .attr('x', function(d) { // sets the x position of the bar
                return chart.xAxis.scale(d.x);
            })
            .attr('y', function(d) { // sets the y position of the bar
                return chart.yAxis.scale(d.y);
            })
            .attr('width', 4) // sets the width of bar
            .attr('height', function(d) { // sets the height of bar
                return (chart.config.height - chart.yAxis.scale(d.y));
            })
            .attr('fill', 'grey');   // fills the bar with grey color

            rect.call(d3.behavior.drag()
              .on("dragstart", this.dragStarted(this))
              .on("drag", this.drag(this))
              .on("dragend", this.dragEnd(this))
             );
        }

        /*
         * Handle dragging
         */
        /**
         * Compute the new x position based on the drag event. Furthermore, update the points position
         * and call event listener to tell that the point has changed.
         */
        getNewPosition( position: number, axis: ChartAxis, point: Point, roundPosition: boolean = false ): number {

            // Compute the scale based on the real size of the axis compare to the theoritical range
            // Because of reveal.js zoom
            var element = axis.group;
            let a = (<any>element.node()).getBoundingClientRect();
            let scale = a.width / axis.scale.range()[1];
            //scale = 1/Reveal.getScale();
            let dragX = position / scale;
            let t = this.chart.xAxis.scale.invert(dragX);
            t = this.chart.xAxis.clipToDomain(t);
            if( roundPosition ) {
                t = Math.round(t); // Round the time
            }
            for( let listener of this.listeners ){
                listener.event(ControlEvent.NEW_TIME, Math.round(t))
            }
            point.x = t;
            let newX = axis.scale(point.x)
            return newX;
        }

        dragStarted: (Chart) => ( (any) => void ) = function(thisBar) {
            return function(d) {
                (<d3.BaseEvent>d3.event).sourceEvent.stopPropagation(); // Prevent other event listener to react
                d3.select(this).classed("dragging", true);
            };
        };

        drag: (Chart) => ( (Point) => void ) = function(thisBar) {
            return function(point) {
                let event: d3.DragEvent = <d3.DragEvent>d3.event;
                thisBar.chart.lastDragEvent = event;
                let newX = thisBar.getNewPosition( event.x, thisBar.chart.xAxis, point, false);
                d3.select(this).attr("x", newX );
            };
        };


        dragEnd: (Chart) => ( (Point) => void ) = function(thisBar) {
            return function(point) {
                d3.select(this).classed("dragging", false);

                let newX = thisBar.getNewPosition( thisBar.chart.lastDragEvent.x, thisBar.chart.xAxis, point, true );
                d3.select(this).attr("x", newX);
            };
        };

        // Listenable "implementation/declaration" (actual implementation comes as a Mixin)
        listeners: EventListener[] = [];
        addListener: (listener: EventListener) => void;
        addListeners: (listeners: EventListener[]) => void;
        clearListeners: () => void;
        notify: (event: ControlEvent, datum?: any) => void;
    }
    applyMixins(TimeBar, [Listenable]);

    export class Line<P extends Point> {
        points: P[];
        lineClass: string;

        constructor( points: P[], lineClass: string){
            this.points = points;
            this.lineClass = lineClass;
        }

        draw( chart: Chart, container: d3.Selection<any> ){
            let lineFunc = d3.svg.line<P>()
            .x(function(d) {
                return chart.xAxis.scale(d.x);
            })
            .y(function(d) {
                return chart.yAxis.scale(d.y);
            })
            .interpolate('linear');

            let inRangePoints = _.filter(this.points, function(p){
                return chart.xAxis.clipToDomain(p.x) == p.x &&
                chart.yAxis.clipToDomain(p.y) == p.y;
            });
            //console.log(inRangePoints)
            container.append('svg:path')
                .attr('d', lineFunc(inRangePoints))
                .attr('class', this.lineClass)
                .attr('stroke', 'blue')
                .attr('stroke-width', 2)
                .attr('fill', 'none');
        }
    }


    export class Chart {
        protected _group: d3.Selection<any>;
        config: ChartConfigWithMargin;
        xAxis: XAxis;
        yAxis: YAxis;
        lines: Line<any>[] = [];

        constructor( config: ChartConfigWithMargin, container: string, xlabel:string, xMin: number, xMax: number, xIsLog, ylabel:string, yMin: number, yMax: number, yIsLog:boolean ) {
            this.config = config;
            this._group = d3.select( container )
            .attr("width", outerWidth)
            .attr("height", outerHeight)
            .append('g')
            .attr("transform", "translate(" + (config.margin.left + config.padding.left) + "," + (config.margin.top  + config.padding.top)+ ")");

            this.xAxis = new XAxis( config, this._group, xMin, xMax, xlabel, xIsLog );
            this.yAxis = new YAxis( config, this._group, yMin, yMax, ylabel, yIsLog );
        }

        public drawAxis() {
            //console.log("Draw series");
            this.xAxis.draw();
            this.yAxis.draw();
        }


        /**
         * Add the line to the chart and draw it
         */
        addLine( line: Line<any> ){
            this.lines.push( line );
            line.draw( this, this._group );
        }

        clearLines() {
            this._group.selectAll("path").remove();
        }

    }

    export class TimeChart extends Chart {
        timeBar: TimeBar;

        constructor( config: ChartConfigWithMargin, container: any, xMin: number, xMax: number, yMin: number, yMax: number, ylabel: string ) {
            super( config, container, "Time", xMin, xMax, false, ylabel, yMin, yMax, false);
            this.timeBar = new TimeBar( this, this._group );
        }

        public draw( t: number ) {
            //console.log("Draw series");
            this.xAxis.draw();
            this.yAxis.draw();
            this.timeBar.draw( t );
        }

        public start() {
            this.draw(0);
        }


        /*
           update() {
           var minDate = d3.min(data, d => d.date);
           var maxDate = d3.max(data, d => d.date);
           var xScale = this._xAxis.update(minDate, maxDate);

           var minScore = d3.min(data, d => d.value);
           var maxScore = d3.max(data, d => d.value);
           var yScale = this._yAxis.update(minScore, maxScore);
           }
         */
    }

    export class ActivityChart extends TimeChart {
        activities: ActiveInterval[];
        cssClass: string;

        constructor( config: ChartConfigWithMargin, container: any, xMin: number, xMax: number, activities: ActiveInterval[], cssClass: string ) {
            super( config, container, xMin, xMax, 0, 1, "");
            this.activities = activities;
            this.cssClass = cssClass;
        }

        draw( t: number ){
            let xMin = this.xAxis.min;
            let xMax = this.xAxis.max;
            let visibleActivities = _.filter( this.activities, function(a) {
                return a.startTime >= xMin && a.endTime <= xMax;
            });

            //console.log("Draw ActivityChart");
            this.xAxis.draw();
            this.timeBar.draw( t );

            var rect = this._group.selectAll('#activity-bar')
            .data(visibleActivities);

            rect.enter()
            .append('rect');


            rect.attr('class', this.cssClass)
            .attr('x', function(d) { // sets the x position of the bar
                return this.xAxis.scale(d.startTime);
            }.bind(this))
            .attr('y', function(d) { // sets the y position of the bar
                return this.yAxis.scale(1);
            }.bind(this))
            .attr('width', ((d) => {
                return this.xAxis.scale(d.endTime) - this.xAxis.scale(d.startTime);
            }).bind(this)) // sets the width of bar
            .attr('height', function(d) { // sets the height of bar
                return (this.config.height - this.yAxis.scale(1));
            }.bind(this))
            .attr('fill', 'black');   // fills the bar with grey color

        }
    }
}
